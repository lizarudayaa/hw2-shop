import './App.css';
import React, {Component} from 'react';
import Header from "./components/Header/Header";
import axios from "axios";
import ItemsList from "./components/Items/ItemsList";
import Footer from "./components/Footer/Footer";
import Modal from "./components/Modal/Modal";
import {modals} from "./components/Modal/ModalDeclaration";
import Loader from "./components/Loader/Loader";

class App extends Component {

    state={
        isLoading : true,
        data : [],
        openModalWindow: null,
        isFavorite : JSON.parse(localStorage.getItem('favorites')) || [] ,
        card : JSON.parse(localStorage.getItem('card')) || [],
        addedItem : []
    }

    componentDidMount() {
        setTimeout(() => {
            axios('./items.json')
                .then(r => this.setState({data : r.data , isLoading: false} ))
        } , 2000)

    }

       openModal = (id) => {
        this.setState({openModalWindow: id})
    }

    closeModal = () => {
        this.setState({openModalWindow: null})
    }

    addNewItemToCard = (data) => {
        this.setState({addedItem : data})
    }

    addToCardBtn = (data) => {
       const {card} = this.state
        const cardItems = card.findIndex(item => data.id === item)
        if (cardItems === -1 ){
            card.push(data.id)
        } else {
            alert('This item was already added to your card')
        }
        localStorage.setItem('card', JSON.stringify(card))
        this.setState({card : card})
    }

    setIsFavorite= (data) => {
        this.setState({isFavorite : data})
    }

    render() {
        const modal = modals.find(i => i.modalID === this.state.openModalWindow)
        return (
            <div className='app-wrapper'>
                <div className='app'>
                    <Header />
                    {this.state.isLoading && <Loader/>}
                    {!this.state.isLoading &&
                    <ItemsList
                        data={this.state.data}
                        isFavorite={this.state.isFavorite}
                        setIsFavorite={this.setIsFavorite}
                        openModal={this.openModal}
                        addCard={this.addNewItemToCard} />
                    }

                    <div>
                        {modal && <Modal
                            closeModal={this.closeModal}
                            header={modal.header}
                            text={modal.text}
                            actions={modal.actions({
                                btnClose: () => {
                                    this.closeModal()
                                },
                                btnAdd: () => {
                                    this.addToCardBtn(this.state.addedItem)
                                    this.closeModal()
                                }
                            })}
                        />}
                    </div>

                </div>

                <Footer />


            </div>







        );
    }
}

export default App;

