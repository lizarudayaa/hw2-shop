import React, {Component} from 'react';
import './Error500.scss'

class Error500 extends Component {
    render() {
        return (
            <div>
                <div className="wrapper-error">
                    <div className="error">
                        <div className="box"></div>
                        <h3>ERROR 500</h3>
                        <p>Things are a little <span>unstable</span> here</p>
                        <p>I suggest come back later</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Error500;