import React, {Component} from 'react';
import {GiBookshelf ,GiShoppingCart } from 'react-icons/gi';
import {BsPerson} from 'react-icons/bs';
import './Header.scss'

class Header extends Component {
    render() {
        return (
            <header>
                <div className="header">
                    <div className="header__logo">
                        <span><GiBookshelf size='7em'/></span>
                    </div>
                    <div className="header__title">
                        <h3>Bookieyork</h3>
                    </div>
                    <div className="header__icons">
                        <span><GiShoppingCart size="2em"/></span>
                        <span><BsPerson size="2em"/></span>
                    </div>
                </div>
            </header>
        );
    }
}

export default Header;