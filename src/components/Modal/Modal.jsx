import React, {Component} from 'react';
import './Modal.scss';
import PropTypes from 'prop-types';

class Modal extends Component {
    render() {
        const {header , text , actions, closeModal} = this.props;
        return (
            <div className='modal' onClick={() => closeModal()}>
                <div className="modal__content"  onClick={e => e.stopPropagation()}>
                    <div className="modal__header">
                        <h3>{header}</h3>
                        <span className="modal__header-close" onClick={() => closeModal()}>X</span>
                    </div>
                    <div className="modal__body">
                        <div className="modal__body-text">
                            <p>{text}</p>
                        </div>
                        <div className="modal__actions">
                            {actions}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    header : PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.object,
    closeModal: PropTypes.func,
}

export default Modal;