import Button from "../Button/Button";

export const modals = [
    {
        modalID:1,
        header: 'Product card',
        text: 'Do you want to add this item to your card?',
        actions: function ({btnClose , btnAdd}){
            return(
                <>
                    <Button text='Add' color='black' onClick={btnAdd} />
                    <Button text='Delete' color='black' onClick={btnClose} />
                </>
            )
        }
    }
]