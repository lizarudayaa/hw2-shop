import React, {Component} from 'react';
import './Loader.scss'
class Loader extends Component {
    render() {
        return (
            <div className="wrapper-loader">
                <div className="cover"></div>
                <div className="page"></div>
                <div className="inner-border"></div>
                <p>LOADING ...</p>
            </div>
        );
    }
}

export default Loader;