import React, {Component} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';



const ButtonStyled = styled.button`
  font-size: 15px;
  width:150px;
  text-transform: uppercase;
  text-align: center;
  padding: 10px 20px;
  margin: 5px;
  color: white;
  border: none;
  background : ${props => props.color}

`


class Button extends Component {
    render() {
        const {color, text, onClick} = this.props;
        return (
            <ButtonStyled color={color} onClick={onClick}>{text}</ButtonStyled>
        );
    }
}
Button.propTypes = {
    color: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
}
export default Button;