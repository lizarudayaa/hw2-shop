import React, {Component} from 'react';
import Button from "../Button/Button";
import Star from "../../theme/icons/star/Star";
import PropTypes from 'prop-types';


class Item extends Component {



    setFavorite= (data) => {
        const {isFavorite , setIsFavorite} = this.props
        const favoriteItems = isFavorite.findIndex(item => data.id === item)
        if (favoriteItems === -1 ){
            isFavorite.push(data.id)
        } else {
            isFavorite.splice(favoriteItems,1)
        }
        localStorage.setItem('favorites', JSON.stringify(isFavorite))
       setIsFavorite(isFavorite)
    }



    render() {


        const {isFavorite, openModal , data , addCard} = this.props

        const buyItem = (id , data) => {
            openModal(id)
            addCard(data)
        }

        return (
            <div className='card'>
                <div className='card__info'>
                    <div className='card__info-img'>
                        <img src={data.img} alt={data.title} className='card_img'/>
                        <span className='card__star' onClick={() => this.setFavorite(data)} >
                            <Star color="yellow" filled={isFavorite.includes(data.id)} />
                        </span>
                    </div>
                    <div className='card__description'>
                        <div className='card__header'>
                            <p className='card__title'>{data.title}</p>
                        </div>
                        <div className='card__body'>
                            <p className='card__price'>{data.price}<span className='card__price_currency'>$</span></p>
                            <Button className='card__btn' color='black' onClick={() => buyItem(1 , data)} text='add to card' />
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
Item.propTypes = {
    data: PropTypes.shape({
        img : PropTypes.string,
        title: PropTypes.string,
        price: PropTypes.number
    }),
    isFavorite : PropTypes.array,
    openModal: PropTypes.func,
    addCard:PropTypes.func,
    setIsFavorite: PropTypes.func

}
export default Item;