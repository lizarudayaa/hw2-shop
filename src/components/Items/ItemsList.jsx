import React, {Component} from 'react';
import Item from "./Item";
import './ItemsList.scss';
import PropTypes from 'prop-types';


class ItemsList extends Component {
    render() {
        const {data , isFavorite , openModal , addCard , setIsFavorite} = this.props;
        const books = data.map(book => <Item key={book.id} data={book} isFavorite={isFavorite} openModal={openModal} addCard={addCard} setIsFavorite={setIsFavorite} />)
        return (
            <div className='cards'>
                {books}
            </div>
        );
    }
}

ItemsList.propTypes = {
    data: PropTypes.array.isRequired,
    isFavorite : PropTypes.array,
    openModal: PropTypes.func,
    addCard:PropTypes.func,
    setIsFavorite: PropTypes.func
}

export default ItemsList;