import React, {Component} from 'react';
import './Footer.scss'
import { AiOutlineTwitter , AiOutlineFacebook, AiOutlineInstagram } from 'react-icons/ai';

class Footer extends Component {
    render() {
        return (
            <footer className='footer'>

                <ul className="footer__list">
                    <li className="footer__list-item"><a href="#home">Home</a></li>
                    <li className="footer__list-item"><a href="#about">About</a></li>
                    <li className="footer__list-item"><a href="#services">Services</a></li>
                    <li className="footer__list-item"><a href="#contact">Contact</a></li>
                </ul>

                <div className="footer__social">
                    <a className="footer__social-item" href="https://twitter.com/?lang=ru">< AiOutlineTwitter/></a>
                    <a className="footer__social-item" href="https://www.facebook.com"><AiOutlineFacebook/></a>
                    <a className="footer__social-item" href="https://www.instagram.com/?hl=ru"><AiOutlineInstagram/></a>
                </div>

                <p className="footer__copyright"> &copy; 2021</p>
            </footer>
        );
    }
}

export default Footer;